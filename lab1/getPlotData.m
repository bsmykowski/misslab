function [xDim, yDim] = getPlotData(v0, alpha)
  xDim = [];
  yDim = [];

  for t = 0:0.05:1.5
    [x, y] = getPoint(t, v0, alpha);

    xDim = [xDim x];
    yDim = [yDim y];
  endfor
endfunction