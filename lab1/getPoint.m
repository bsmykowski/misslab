function [x, y] = getPoint(t, v0, alpha)
  x = getX(t, v0, alpha);
  y = getY(t, v0, alpha);
endfunction