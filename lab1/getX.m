function retval = getX(t, v0, alpha)
  retval = t*v0*cos(alpha);
endfunction