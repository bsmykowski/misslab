function retval = getX(t, v0, alpha)
  retval = t*v0*sin(alpha) - 10*t*t/2;
endfunction