hold on;

allowedRegion = [2 1 2 4; 4 1 7 1];
[nr, nc] = size (allowedRegion)
for row = 1:nr
  plot(allowedRegion(row, [1, 3]), allowedRegion(row, [2, 4]));
endfor

for v0 = 1:2:10
  for alpha = 0:0.5:pi/2
    [xDim, yDim] = getPlotData(v0, alpha);
    plot(xDim, yDim);
  endfor
endfor
