function distance = distance(X, Y, centerPoint)
  distance = sqrt((X-centerPoint(1)).^2+(Y-centerPoint(2)).^2);
endfunction