xRange = 0:.1:3;
yRange = -3:.1:1;
[X, Y] = meshgrid(xRange, yRange);
beta = 1;

numerOfSamples = size(ft)(1);

radialValues=[];
for i = 1:numerOfSamples
  for j = 1:numerOfSamples
    radialValues(i, j) = radialFunction(xt(i), yt(i), [xt(j), yt(j)], beta);
  endfor
endfor

aCoefficients = radialValues\ft;

Z = getZValue(X, Y, xt, yt, beta, aCoefficients);

hold on;
surf(X, Y, Z);
scatter3(xt, yt, ft);


testValues = getZValue(xv, yv, xt, yt, beta, aCoefficients);
errors = abs(testValues- fv);

numberOfTestSamples = size(fv)(1);
errorMax=max(errors)
errorMean = sum(errors)/numberOfTestSamples

