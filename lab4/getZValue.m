function Z = getZValue(X, Y, Xi, Yi, beta, aCoefficients)
  numberOfSamples = size(aCoefficients)(1);
  Z = zeros(size(X)(1), size(Y)(2));
  for k=1:numberOfSamples
    Z = Z.+ aCoefficients(k).*radialFunction(X, Y, [Xi(k), Yi(k)], beta);
  endfor
endfunction