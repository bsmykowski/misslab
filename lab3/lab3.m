mu=0.01; 
tau=100; 
b=0.5; 
beta=b/2/mu; 
L=1; 
M=100; 
N=2000; 
x=linspace(0,L,M)';
v=sqrt(tau/mu); 
dx=x(2)-x(1); 
dt=dx/v; 
p=(v*dt/dx)^2; 
q=1+beta*dt;
u=1-beta*dt; 
s=-2*x.^2+.8*x.+.92; 
g=sin(x*2*pi); 
%l=zeros(1,N)+0.92; 
h=zeros(1,N)+.8;
r=zeros(1,N)-0.28;


f(:,1)=s;
f(1,2)=p*(f(2,1)-f(1,1)-dx*h(1)) + f(1,1) +dt*g(1);
f(2:M-1,2)=p/2*(f(3:M,1)-2*f(2:M-1,1)+ f(1:M-2,1))+f(2:M-1,1)+u*dt*g(2:M-1);
f(M,2)=r(2);
for n=2:N-1
  f(1,n+1)=2*p*(f(2,n)-f(1,n)-dx*h(n) )+2*f(1,n)-f(1,n-1);
  f(2:M-1,n+1)=p/q*(f(3:M,n)-2*f(2:M-1,n)+f(1:M-2,n))+2/q*f(2:M-1,n)-u/q*f(2:M-1,n-1);
  f(M,n+1)=r(n+1);
end

%mesh(f)

hold on;

fe = .8*x - 1.08;
plot(fe);
plot(f(:,N));

%for i=1:N
%  plot(f(1:M, i));
%  xlim([0 100]);
%  ylim([-3 3]);
%  drawnow;
%  i
%  pause(.01);
%end




