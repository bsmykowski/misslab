hold;

x0=-5;
y0=0;
z0=200;
position=[x0,y0,z0];
v0x=2;
v0y=4;
v0z=0;
velocity=[v0x, v0y, v0z];
k=.8;

range = -15:0.5:15;
[X, Y] = meshgrid(range, range);
Z = surfaceFunction(X, Y);
results = [];

for iteration = 1:5
  resultsRow = [];
  tI = impactTime(position, velocity);

  trajectoryX=[];
  trajectoryY=[];
  trajectoryZ=[];
  for time = 0:0.1:tI
    tmpPosition = getNewPosition(time, velocity, position);
    trajectoryX=[trajectoryX tmpPosition(1)];
    trajectoryY=[trajectoryY tmpPosition(2)];
    trajectoryZ=[trajectoryZ tmpPosition(3)];
  end
  plot3(trajectoryX, trajectoryY, trajectoryZ, 'r-', 'linewidth', 10);
 
  position = getNewPosition(tI, velocity, position);
  n = getN(position(1), position(2));
  impactVelocity=[velocity(1), velocity(2), velocity(3)-10*tI];
  velocity = getNewVelocity(impactVelocity, n, k);

  kineticEnergy = norm(impactVelocity)^2/2;
  potentialEnergy = 10*position(3);
  totalEnergy = kineticEnergy+potentialEnergy;
  
  resultsRow = [position(1), position(2), position(3), tI, kineticEnergy, potentialEnergy, totalEnergy];
  results = [results; resultsRow];
  
  scatter3(position(1), position(2), position(3), [], "r", "filled");
end
  
mesh(X, Y, Z);

results

