function retval = getZ(t, v0z, z0)
  retval = v0z*t+z0-5*t^2;
endfunction