function retval = getNewVelocity(v0, n, k)
  retval = sqrt(k)*(v0-2*dot(v0, n)*n);
endfunction