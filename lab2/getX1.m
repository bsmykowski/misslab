function retval = getX1(a, b, delta)
  retval = (-b-sqrt(delta))/(2*a);
endfunction