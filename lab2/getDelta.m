function retval = getDelta(a, b, c)
  retval = b^2 - 4*a*c;
endfunction