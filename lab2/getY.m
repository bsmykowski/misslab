function retval = getY(t, v0y, y0)
  retval = v0y*t+y0;
endfunction