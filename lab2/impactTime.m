function retval = impactTime(position, velocity)
  v0x=velocity(1);
  v0y=velocity(2);
  v0z=velocity(3);
  x0=position(1);
  y0=position(2);
  z0=position(3);
  a=-5-v0x^2-v0y^2;
  b=v0z-2*x0*v0x-2*y0*v0y;
  c=z0-x0^2-y0^2;
  delta = getDelta(a, b, c);
  t1 = getX1(a, b, delta);
  t2 = getX2(a, b, delta);
  retval = max([t1, t2]);
endfunction