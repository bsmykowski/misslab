function retval = surfaceFunction(x,y)
  retval = x.^2 + y.^2;
endfunction