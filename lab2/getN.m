function retval = getN(x, y)
  N = sqrt((2*x)^2 + (2*y)^2 +1);
  retval = [-2*x, -2*y, 1]/N;
endfunction