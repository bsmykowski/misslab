function point = getNewPosition(time, velocity, position)
  x=getX(time, velocity(1), position(1));
  y=getY(time, velocity(2), position(2));
  z=getZ(time, velocity(3), position(3));
  point = [x, y, z];
endfunction