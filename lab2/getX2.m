function retval = getX2(a, b, delta)
  retval = (-b+sqrt(delta))/(2*a);
endfunction